package atrbac_ethereum.convert;

import java.io.*;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;

public class BakedConverter {

    /**
     * 
     * @param m
     * @param solidityFile
     * @return TRUE if successful, FALSE otherwise
     */
    public static boolean convert(MohawkT m, File solidityFile) {
        BufferedWriter w = null;
        try {
            w = new BufferedWriter(new FileWriter(solidityFile));
            int c = 1;
            w.write(start_template);

            w.write("\n\t" + seperator_comment + "\n\t// Safety Query\n");
            w.write(safetyQuery(m, "\t"));
            w.write("\n\t" + seperator_comment);

            w.write("\n\n\n\t" + seperator_comment + "\n\t// CanAssign Rules\n");
            c = 1;
            for (Rule r : m.canAssign.getRules()) {
                w.write(canAssignRule(m.roleHelper, m.timeIntervalHelper, r, c, "\t"));
                c++;
            }
            w.write("\n\t" + seperator_comment);

            w.write("\n\n\n\t" + seperator_comment + "\n\t// CanRevoke Rules\n");
            c = 1;
            for (Rule r : m.canRevoke.getRules()) {
                w.write(canRevokeRule(m.roleHelper, m.timeIntervalHelper, r, c, "\t"));
                c++;
            }
            w.write("\n\t" + seperator_comment);

            // CanEnable Rules
            w.write("\n\n\n\t" + seperator_comment + "\n\t// CanEnable Rules\n");
            c = 1;
            for (Rule r : m.canEnable.getRules()) {
                w.write(canEnableRule(m.roleHelper, m.timeIntervalHelper, r, c, "\t"));
                c++;
            }
            w.write("\n\t" + seperator_comment);

            w.write("\n\n\n\t" + seperator_comment + "\n\t// CanDisable Rules\n");
            c = 1;
            for (Rule r : m.canDisable.getRules()) {
                w.write(canDisableRule(m.roleHelper, m.timeIntervalHelper, r, c, "\t"));
                c++;
            }
            w.write("\n\t" + seperator_comment);

            w.write(end_template);

            w.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (w != null) {
                try {
                    w.close();
                } catch (IOException e) {
                }
            }
        }

        return true;
    }

    public static String safetyQuery(MohawkT m, String linePrefix) {
        StringBuilder sb = new StringBuilder();

        sb.append(linePrefix).append("constructor() public { owner = msg.sender; }\n");
        sb.append(linePrefix).append("address payable owner;\n");
        sb.append(linePrefix).append("function deposit(uint256 amount) public payable {\n");
        sb.append(linePrefix + "\t").append("require(msg.value == amount, \"Incorrect amount sent\");\n");
        sb.append(linePrefix).append("}\n\n");

        String query = "";
        int timeslot = m.timeIntervalHelper.indexOfReduced(m.query._timeslot).first();
        boolean first = true;
        for (Role r : m.query._roles) {
            String precond = "true";
            if (r.isNegativePrecondition()) precond = "false";
            if (first == false) {
                query += " && ";
            }
            first = false;

            query += "trbac_state[msg.sender][" + Math.abs(m.roleHelper.indexOf(r)) + "][" + timeslot + "] == "
                    + precond;
        }

        sb.append(linePrefix).append("function withdraw(uint256 amount) public {\n");
        sb.append(linePrefix + "\t").append("require(amount <= address(this).balance, \"Not Enough Funds\");\n");
        sb.append(linePrefix + "\t").append("require(" + query + ", \"User does not satisfy the Safety Query\");\n");
        sb.append(linePrefix + "\t").append("msg.sender.transfer(amount);\n");
        sb.append(linePrefix).append("}\n");

        return sb.toString();
    }

    public static String canAssignRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return caCRRule(rh, tih, r, ruleIndex, linePrefix, true);
    }
    public static String canRevokeRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return caCRRule(rh, tih, r, ruleIndex, linePrefix, false);
    }
    /**
     * 
     * @param r
     * @param canAssign
     * @return
     */
    public static String caCRRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex, String linePrefix,
            boolean canAssign) {
        StringBuilder sb = new StringBuilder();
        String lp = linePrefix + "\t";
        String cacr = "CA";
        String setTo = "true";
        if (canAssign == false) {
            cacr = "CR";
            setTo = "false";
        }

        sb.append(linePrefix).append("// " + r.getString(ruleIndex, 0) + "\n");
        sb.append(linePrefix).append(
                "function fire" + cacr + ruleIndex + "Rule(address _targetUser) public noZeroAddress(_targetUser) \n");
        sb.append(linePrefix).append("{\n");
        sb.append(adminCondition(rh, tih, r, lp));

        if (r._preconditions.size() > 0) {
            if (r._roleSchedule.size() > 1) {
                String rs = "";
                for (TimeSlot ts : r._roleSchedule) {
                    rs += tih.indexOfReduced(ts).first() + ",";
                }
                rs = rs.substring(0, rs.length() - 1);
                sb.append(lp).append("uint[] roleSchedule = [" + rs + "];\n");
                sb.append(lp).append("for(uint ts=0;ts<" + r._roleSchedule.size() + ";ts++) {\n");
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp + "\t").append("target(trbac_state[_targetUser][" + Math.abs(rh.indexOf(ro))
                            + "][roleSchedule[ts]] == " + precond + ");\n");
                }
                sb.append(lp).append("}\n\n");
            } else if (r._roleSchedule.size() == 1) {
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp).append("target(trbac_state[_targetUser][" + Math.abs(rh.indexOf(ro)) + "]["
                            + tih.indexOfReduced(r._roleSchedule.get(0)).first() + "] == " + precond + ");\n");
                }
            }
        }

        for (TimeSlot ts : r._roleSchedule) {
            sb.append(lp).append("trbac_state[_targetUser][" + rh.indexOf(r._role) + "]["
                    + tih.indexOfReduced(ts).first() + "] = " + setTo + ";\n");
        }
        sb.append(linePrefix).append("}\n\n");

        return sb.toString();
    }

    public static String canEnableRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return ceCDRule(rh, tih, r, ruleIndex, linePrefix, true);
    }
    public static String canDisableRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return ceCDRule(rh, tih, r, ruleIndex, linePrefix, false);
    }
    /**
     * 
     * @param r
     * @param canAssign
     * @return
     */
    public static String ceCDRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex, String linePrefix,
            boolean canEnable) {
        StringBuilder sb = new StringBuilder();
        String lp = linePrefix + "\t";
        String cecd = "CE";
        String setTo = "true";
        if (canEnable == false) {
            cecd = "CD";
            setTo = "false";
        }

        sb.append(linePrefix).append("function fire" + cecd + ruleIndex + "Rule() public {\n");
        sb.append(lp).append("// " + r.getString(ruleIndex, 0) + "\n");
        sb.append(adminCondition(rh, tih, r, lp));

        if (r._preconditions.size() > 0) {
            if (r._roleSchedule.size() > 1) {
                String rs = "";
                for (TimeSlot ts : r._roleSchedule) {
                    rs += tih.indexOfReduced(ts).first() + ",";
                }
                rs = rs.substring(0, rs.length() - 1);
                sb.append(lp).append("uint[] roleSchedule = [" + rs + "];\n");
                sb.append(lp).append("for(uint ts=0;ts<" + r._roleSchedule.size() + ";ts++) {\n");
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp + "\t").append("target(role_enablement[" + Math.abs(rh.indexOf(ro))
                            + "][roleSchedule[ts]] == " + precond + ");\n");
                }
                sb.append(lp).append("}\n\n");
            } else if (r._roleSchedule.size() == 1) {
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp).append("target(role_enablement[" + Math.abs(rh.indexOf(ro)) + "]["
                            + tih.indexOfReduced(r._roleSchedule.get(0)).first() + "] == " + precond + ");\n");
                }
            }
        }

        for (TimeSlot ts : r._roleSchedule) {
            sb.append(lp).append("role_enablement[" + rh.indexOf(r._role) + "][" + tih.indexOfReduced(ts).first()
                    + "] = " + setTo + ";\n");
        }
        sb.append(linePrefix).append("}\n\n");

        return sb.toString();
    }

    public static String adminCondition(RoleHelper rh, TimeIntervalHelper tih, Rule r, String lp) {
        if (r._adminRole.isAllRoles()) return "";

        StringBuilder sb = new StringBuilder();

        int adminrole = rh.indexOf(r._adminRole);

        sb.append(lp).append("sender(");
        boolean first = true;
        for (int ts : tih.indexOfReduced(r._adminTimeInterval)) {
            if (first == false) {
                sb.append(" || ");
            }
            first = false;
            sb.append("trbac_state[msg.sender][" + adminrole + "][" + ts + "] == true");
        }
        sb.append(");\n");

        return sb.toString();
    }

    public static String start_template = "pragma solidity ^0.5.0;\n\ncontract ATRBAC {\n"
            + "\tmapping(uint => mapping(uint => bool)) public role_enablement;\n"
            + "\tmapping (address => mapping (uint => mapping (uint => bool))) trbac_state;\n\n"
            + "\tmodifier noZeroAddress(address account) {\n"
            + "\t\trequire(account != address(0), \"Address cannot be the zero address\");\n\t\t_;\n\t}\n"
            + "\tfunction target(bool cond) public pure {\n"
            + "\t\trequire(cond, \"Target User does not satisfy the preconditions for the rule.\");\n\t}\n"
            + "\tfunction sender(bool cond) public pure {\n"
            + "\t\trequire(cond, \"Sender is not authorized to fire this rule at this time.\");\n\t}\n"
            + "\tfunction role(bool cond) public pure {\n"
            + "\t\trequire(cond, \"Target Role does not satisfy the preconditions for the rule.\");\n\t}\n";

    public static String end_template = "\n}";

    public static String seperator_comment = "//////////////////////////////////////////////////////////";

}
