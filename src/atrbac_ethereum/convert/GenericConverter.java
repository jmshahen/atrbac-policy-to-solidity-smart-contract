package atrbac_ethereum.convert;

import java.io.*;
import java.util.ArrayList;

import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;

public class GenericConverter {

    /**
     * 
     * @param m
     * @param solidityFile
     * @return TRUE if successful, FALSE otherwise
     */
    public static boolean convert(MohawkT m, File solidityFile) {
        BufferedWriter w = null;
        try {
            w = new BufferedWriter(new FileWriter(solidityFile));

            w.write(start_template);
            w.write("   console.log('Running " + m.policyFilename.replace("\\", "/") + "');\n");
            w.write(varDeploy);

            w.write(createRoles(m, "  "));
            w.write(createTimeslots(m, "  "));

            w.write(createCanRules(m, "CA", "  "));
            w.write(createCanRules(m, "CR", "  "));
            w.write(createCanRules(m, "CE", "  "));
            w.write(createCanRules(m, "CD", "  "));

            w.write(end_template);

            w.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (w != null) {
                try {
                    w.close();
                } catch (IOException e) {
                }
            }
        }

        return true;
    }

    public static String createRoles(MohawkT m, String tab) {
        StringBuilder sb = new StringBuilder();
        m.roleHelper.allowZeroRole = false; // TRUE is at position 0
        m.roleHelper.setupSortedRoles();

        sb.append("\n  ").append(seperator_comment).append("\n  // Create Roles\n");
        for (int i = 1; i < m.numberOfRoles(); i++) {
            sb.append(tab).append("trans = await instance.suAddNewRole.sendTransaction('")
                    .append(m.roleHelper.get(i).getName()).append("', { from: su });\n");
            sb.append(tab).append(each_gas_template);
        }
        sb.append(tab).append("console.log('[createRoles] Total Gas used:', gas);\n");
        sb.append(tab).append(total_gas_template);
        sb.append("\n  ").append(seperator_comment).append("\n");

        return sb.toString();
    }

    public static String createTimeslots(MohawkT m, String tab) {
        StringBuilder sb = new StringBuilder();
        m.timeIntervalHelper.allowZeroTimeslot = true;
        m.timeIntervalHelper.reduceToTimeslots();

        sb.append("\n  ").append(seperator_comment).append("\n  // Create Timeslots\n");
        for (int i = 0; i < m.numberOfTimeslots(); i++) {
            sb.append(tab).append("trans = await instance.suAddTimeSlot.sendTransaction(").append(i).append(", ")
                    .append(i + 1).append(", { from: su });\n");
            sb.append(tab).append(each_gas_template);
        }
        sb.append(tab).append("console.log('[createTimeslots] Total Gas used:', gas);\n");
        sb.append(tab).append(total_gas_template);
        sb.append("\n  ").append(seperator_comment).append("\n");

        return sb.toString();
    }

    /**
     * 
     * @param m the mohawk file
     * @param ruleType can be: {CA,CR,CE,CD}
     * @param tab line prefix (usually '\t' or '  ')
     * @return
     */
    public static String createCanRules(MohawkT m, String ruleType, String tab) {
        StringBuilder sb = new StringBuilder();
        RoleHelper rh = m.roleHelper;
        sb.append("\n  ").append(seperator_comment).append("\n  // Create ").append(ruleType).append(" Rules\n");

        ArrayList<Rule> rules;
        switch (ruleType) {
            case "CA" :
                rules = m.canAssign.getRules();
                break;
            case "CR" :
                rules = m.canRevoke.getRules();
                break;
            case "CE" :
                rules = m.canEnable.getRules();
                break;
            case "CD" :
                rules = m.canDisable.getRules();
                break;
            default :
                throw new IllegalArgumentException("ruleType must be: CA/CR/CE/CD");
        }

        for (Rule r : rules) {
            sb.append(tab).append("// ").append(r.toString()).append("\n");
            sb.append(tab).append("trans = await instance.suAdd").append(ruleType).append("Rule.sendTransaction(");
            // Admin Role
            if (r._adminRole.isAllRoles()) {
                sb.append("0, ");
            } else {
                sb.append(rh._hashedRoles.get(r._adminRole.getString())).append(", ");
            }
            // Admin time interval
            sb.append(r._adminTimeInterval._start).append(", ").append(r._adminTimeInterval._finish).append(", ")
                    // Precondition
                    .append(precondtionString(m, r)).append(", ")
                    // Timeslot Array
                    .append(timeslotArrayString(m, r)).append(", ")
                    // Target User
                    .append(rh._hashedRoles.get(r._role.getString())).append(", ")
                    //
                    .append(" { from: su });\n");
            sb.append(tab).append(each_gas_template);
        }
        sb.append(tab).append("console.log('[create").append(ruleType).append("Rules] Total Gas used:', gas);\n");
        sb.append(tab).append(total_gas_template);
        sb.append("\n  ").append(seperator_comment).append("\n");

        return sb.toString();
    }

    public static String precondtionString(MohawkT m, Rule r) {
        // [-1 * rid['ECE602_TA'], rid['Student']]
        if (r._preconditions.size() == 0) { return "[]"; }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        boolean first = true;

        for (Role ro : r._preconditions) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            if (ro.isNegativePrecondition()) {
                sb.append("-1 * ");
            }
            sb.append(m.roleHelper._hashedRoles.get(ro.getName()));
        }

        sb.append("]");

        return sb.toString();
    }

    public static String timeslotArrayString(MohawkT m, Rule r) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        boolean first = true;
        for (TimeSlot ts : r._roleSchedule) {
            if (first) {
                first = false;
            } else {
                sb.append(", ");
            }
            sb.append(ts.getStartTime());
        }

        sb.append("]");

        return sb.toString();
    }

    public static String safetyQuery(MohawkT m, String linePrefix) {
        StringBuilder sb = new StringBuilder();

        sb.append(linePrefix).append("constructor() public { owner = msg.sender; }\n");
        sb.append(linePrefix).append("address payable owner;\n");
        sb.append(linePrefix).append("function deposit(uint256 amount) public payable {\n");
        sb.append(linePrefix + "  ").append("require(msg.value == amount, \"Incorrect amount sent\");\n");
        sb.append(linePrefix).append("}\n\n");

        String query = "";
        int timeslot = m.timeIntervalHelper.indexOfReduced(m.query._timeslot).first();
        boolean first = true;
        for (Role r : m.query._roles) {
            String precond = "true";
            if (r.isNegativePrecondition()) precond = "false";
            if (first == false) {
                query += " && ";
            }
            first = false;

            query += "trbac_state[msg.sender][" + Math.abs(m.roleHelper.indexOf(r)) + "][" + timeslot + "] == "
                    + precond;
        }

        sb.append(linePrefix).append("function withdraw(uint256 amount) public {\n");
        sb.append(linePrefix + "  ").append("require(amount <= address(this).balance, \"Not Enough Funds\");\n");
        sb.append(linePrefix + "  ").append("require(" + query + ", \"User does not satisfy the Safety Query\");\n");
        sb.append(linePrefix + "  ").append("msg.sender.transfer(amount);\n");
        sb.append(linePrefix).append("}\n");

        return sb.toString();
    }

    public static String canAssignRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return caCRRule(rh, tih, r, ruleIndex, linePrefix, true);
    }
    public static String canRevokeRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return caCRRule(rh, tih, r, ruleIndex, linePrefix, false);
    }
    /**
     * 
     * @param r
     * @param canAssign
     * @return
     */
    public static String caCRRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex, String linePrefix,
            boolean canAssign) {
        StringBuilder sb = new StringBuilder();
        String lp = linePrefix + "  ";
        String cacr = "CA";
        String setTo = "true";
        if (canAssign == false) {
            cacr = "CR";
            setTo = "false";
        }

        sb.append(linePrefix).append(
                "function fire" + cacr + ruleIndex + "Rule(address _targetUser) public noZeroAddress(_targetUser) {\n");
        sb.append(lp).append("// " + r.getString(ruleIndex, 0) + "\n");
        sb.append(adminCondition(rh, tih, r, lp));

        if (r._preconditions.size() > 0) {
            if (r._roleSchedule.size() > 1) {
                String rs = "";
                for (TimeSlot ts : r._roleSchedule) {
                    rs += tih.indexOfReduced(ts).first() + ",";
                }
                rs = rs.substring(0, rs.length() - 1);
                sb.append(lp).append("uint[] roleSchedule = [" + rs + "];\n");
                sb.append(lp).append("for(uint ts=0;ts<" + r._roleSchedule.size() + ";ts++) {\n");
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp + "  ")
                            .append("require(trbac_state[_targetUser][" + Math.abs(rh.indexOf(ro))
                                    + "][roleSchedule[ts]] == " + precond
                                    + ",\"Target User does not satisfy the preconditions for the rule.\");\n");
                }
                sb.append(lp).append("}\n\n");
            } else if (r._roleSchedule.size() == 1) {
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp)
                            .append("require(trbac_state[_targetUser][" + Math.abs(rh.indexOf(ro)) + "]["
                                    + tih.indexOfReduced(r._roleSchedule.get(0)).first() + "] == " + precond
                                    + ",\"Target User does not satisfy the preconditions for the rule.\");\n");
                }
            }
        }

        for (TimeSlot ts : r._roleSchedule) {
            sb.append(lp).append("trbac_state[_targetUser][" + rh.indexOf(r._role) + "]["
                    + tih.indexOfReduced(ts).first() + "] = " + setTo + ";\n");
        }
        sb.append(linePrefix).append("}\n\n");

        return sb.toString();
    }

    public static String canEnableRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return ceCDRule(rh, tih, r, ruleIndex, linePrefix, true);
    }
    public static String canDisableRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex,
            String linePrefix) {
        return ceCDRule(rh, tih, r, ruleIndex, linePrefix, false);
    }
    /**
     * 
     * @param r
     * @param canAssign
     * @return
     */
    public static String ceCDRule(RoleHelper rh, TimeIntervalHelper tih, Rule r, int ruleIndex, String linePrefix,
            boolean canEnable) {
        StringBuilder sb = new StringBuilder();
        String lp = linePrefix + "  ";
        String cecd = "CE";
        String setTo = "true";
        if (canEnable == false) {
            cecd = "CD";
            setTo = "false";
        }

        sb.append(linePrefix).append("function fire" + cecd + ruleIndex + "Rule() public {\n");
        sb.append(lp).append("// " + r.getString(ruleIndex, 0) + "\n");
        sb.append(adminCondition(rh, tih, r, lp));

        if (r._preconditions.size() > 0) {
            if (r._roleSchedule.size() > 1) {
                String rs = "";
                for (TimeSlot ts : r._roleSchedule) {
                    rs += tih.indexOfReduced(ts).first() + ",";
                }
                rs = rs.substring(0, rs.length() - 1);
                sb.append(lp).append("uint[] roleSchedule = [" + rs + "];\n");
                sb.append(lp).append("for(uint ts=0;ts<" + r._roleSchedule.size() + ";ts++) {\n");
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp + "  ")
                            .append("require(role_enablement[" + Math.abs(rh.indexOf(ro)) + "][roleSchedule[ts]] == "
                                    + precond
                                    + ",\"Target Role does not satisfy the preconditions for the rule.\");\n");
                }
                sb.append(lp).append("}\n\n");
            } else if (r._roleSchedule.size() == 1) {
                for (Role ro : r._preconditions) {
                    String precond = "true";
                    if (ro.isNegativePrecondition()) precond = "false";

                    sb.append(lp)
                            .append("require(role_enablement[" + Math.abs(rh.indexOf(ro)) + "]["
                                    + tih.indexOfReduced(r._roleSchedule.get(0)).first() + "] == " + precond
                                    + ",\"Target Role does not satisfy the preconditions for the rule.\");\n");
                }
            }
        }

        for (TimeSlot ts : r._roleSchedule) {
            sb.append(lp).append("role_enablement[" + rh.indexOf(r._role) + "][" + tih.indexOfReduced(ts).first()
                    + "] = " + setTo + ";\n");
        }
        sb.append(linePrefix).append("}\n\n");

        return sb.toString();
    }

    public static Object adminCondition(RoleHelper rh, TimeIntervalHelper tih, Rule r, String lp) {
        if (r._adminRole.isAllRoles()) return "";

        StringBuilder sb = new StringBuilder();

        int adminrole = rh.indexOf(r._adminRole);

        sb.append(lp).append("require(");
        boolean first = true;
        for (int ts : tih.indexOfReduced(r._adminTimeInterval)) {
            if (first == false) {
                sb.append(" || ");
            }
            first = false;
            sb.append("trbac_state[msg.sender][" + adminrole + "][" + ts + "] == true");
        }
        sb.append(", \"Sender is not authorized to fire this rule at this time.\");\n");

        return sb.toString();
    }

    public static String start_template = "var atrbac = artifacts.require('ATRBAC');\n"
            + "const truffleAssert = require('truffle-assertions');\nmodule.exports = async function(callback) {\n";
    public static String varDeploy = "  let accounts = await web3.eth.getAccounts();\n"
            + "  var su = accounts[0];\n  let instance;\n  let trans;\n  let totalGas=0;\n  let allGas=[];\n"
            + "  let gas=0;\n\n  instance = await atrbac.deployed();\n"
            + "  let deployGas = await atrbac.new.estimateGas();\n  console.log('Deployed Gas:', deployGas);\n"
            + "  totalGas+=deployGas; allGas.push(deployGas);\n\n";

    public static String end_template = "  console.log('Total Gas:', totalGas);\n"
            + "  console.log('All Gas:', allGas);\n  callback();\n};";
    public static String each_gas_template = "gas += trans.receipt.gasUsed;\n";
    public static String total_gas_template = "totalGas+=gas; allGas.push(gas); gas = 0;\n";

    public static String seperator_comment = "//////////////////////////////////////////////////////////";

}
