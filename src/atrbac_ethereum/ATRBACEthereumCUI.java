package atrbac_ethereum;

import java.io.*;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class ATRBACEthereumCUI {
    public static final Logger logger = Logger.getLogger("shahen");
    public static String previousCommandFilename = "ATRBACEthereumCUIPreviousCommand.txt";
    public static String previousCmd;

    public static void main(String[] args) {
        ATRBACEthereumInstance inst = new ATRBACEthereumInstance();
        ArrayList<String> argv = new ArrayList<String>();
        ArrayList<String[]> cmds = new ArrayList<String[]>();
        String arg = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.println("HELP: To solve all test cases: !run_all");
        System.out.println("HELP: Use '!e' to separate commands, and '!x' to run the previous commands");
        System.out.print("Enter Commandline Argument: ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            arg = user_input.next();

            if (quotedStr.isEmpty() && arg.startsWith("\"")) {
                System.out.println("Starting: " + arg);
                quotedStr = arg.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && arg.endsWith("\"")) {
                System.out.println("Ending: " + arg);
                argv.add(quotedStr + " " + arg.substring(0, arg.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + arg;
                continue;
            }

            // !e indicates that a command is finished and that a new command is wanted
            if (arg.equals("!e") || arg.equals("!E")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                continue;
            }

            if (arg.equals("!p!x") || arg.equals("!P!X")) {
                Collections.addAll(argv, previousCmd.split(" "));
                fullCommand.append(previousCmd + " ");
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                break;
            }

            // !x indicates that no other input should be read, and that the commands should be executed
            if (arg.equals("!x") || arg.equals("!X")) {
                if (argv.size() > 0) {
                    cmds.add(argv.toArray(new String[1]));
                    argv.clear();
                }
                break;
            }

            if (arg.equals("!p") || arg.equals("!P")) {
                Collections.addAll(argv, previousCmd.split(" "));
                fullCommand.append(previousCmd + " ");
                continue;
            }

            // if (arg.equals("!run_all")) {
            // argv.clear();
            // cmds = allStats();
            // break;
            // }

            fullCommand.append(arg + " ");
            argv.add(arg);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        if (!arg.equals("!run_all")) {
            try {
                FileWriter fw;
                fw = new FileWriter(previousCommandFilename, false);
                fw.write(fullCommand.toString().replaceAll("!p", previousCmd));
                fw.close();
            } catch (IOException e) {
                System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
            }
        }

        long fullstart = System.currentTimeMillis();
        for (String[] c : cmds) {
            System.out.println("==============================================================");
            System.out.println("== CMD: " + Arrays.toString(c));
            System.out.println("==================START OF OUTPUT=============================");
            long start = System.currentTimeMillis();
            inst = new ATRBACEthereumInstance();
            inst.run(c);
            Duration d = Duration.ofMillis(System.currentTimeMillis() - start);
            System.out.println("====================END OF OUTPUT============================");
            System.out.println("Done [" + humanReadableFormat(d) + "]");
            System.out.println("");
        }

        if (cmds.size() > 1) {
            Duration d = Duration.ofMillis(System.currentTimeMillis() - fullstart);
            System.out.println("\n");
            System.out.println("===========================");
            System.out.println("===========================");
            System.out.println("Done all Tests [" + humanReadableFormat(d) + "]");
        }
    }

    public static String humanReadableFormat(Duration duration) {
        return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
    }

    public static ArrayList<String[]> allStats() {
        ArrayList<String[]> cmds = new ArrayList<>();
        String stats = "-run baked -loglevel quiet -bulk ";
        // From original Mohawk paper
        cmds.add((stats + "-input data/mohawkT/Mohawk/positive").split(" "));
        cmds.add((stats + "-input data/mohawkT/Mohawk/mixednocr").split(" "));
        cmds.add((stats + "-input data/mohawkT/Mohawk/mixed").split(" "));

        // Rainse Testcases
        cmds.add((stats + "-input data/mohawkT/ranise/testsuiteb/").split(" "));
        cmds.add((stats + "-input data/mohawkT/ranise/testsuitec/hos/").split(" "));
        cmds.add((stats + "-input data/mohawkT/ranise/testsuitec/univ/").split(" "));

        // Uzun Testcases
        cmds.add((stats + "-input data/mohawkT/uzun/roles/").split(" "));
        cmds.add((stats + "-input data/mohawkT/uzun/rules/").split(" "));
        cmds.add((stats + "-input data/mohawkT/uzun/timeslots/").split(" "));

        return cmds;
    }
    public static void printCommonCommands() {
        String allcmd = "-run baked -loglevel quiet -bulk ";
        System.out.println("\n\n--- Common Commands ---");
        System.out.println(allcmd + "-input data/mohawkT/regression/reachable/ !x");
        System.out.println(allcmd + "-input data/mohawkT/regression/unreachable/ !x");

        System.out.println("\n###################################################################");
        System.out.println("--- From original Mohawk paper ---");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/positive !x");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/mixednocr !x");
        System.out.println(allcmd + "-input data/mohawkT/mohawk/mixed !x");
        System.out.println("");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuiteb !x");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuitec/hos !x");
        System.out.println(allcmd + "-input data/mohawkT/ranise/testsuitec/univ !x");
        System.out.println("");
        System.out.println(allcmd + "-input data/mohawkT/uzun/roles !x");
        System.out.println(allcmd + "-input data/mohawkT/uzun/rules !x");
        System.out.println(allcmd + "-input data/mohawkT/uzun/timeslots !x");
        System.out.println("\n###################################################################");
        System.out.println("--- Manual Tests ---");
        System.out.println("-run baked -input data/mohawkT/regression/reachable/reachable-test06.mohawkT !x");
        System.out.println("-run generic -input data/mohawkT/regression/reachable/reachable-test06.mohawkT !x");

        System.out.println("\n-----------------------------------------------");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command ('!p' expands to this): " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
