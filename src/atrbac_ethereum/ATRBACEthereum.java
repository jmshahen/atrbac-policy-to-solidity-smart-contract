package atrbac_ethereum;

import java.util.logging.Logger;

public class ATRBACEthereum {
    public static final Logger logger = Logger.getLogger("shahen");

    public static void main(String[] args) {
        ATRBACEthereumInstance inst = new ATRBACEthereumInstance();

        inst.run(args);
    }

}
