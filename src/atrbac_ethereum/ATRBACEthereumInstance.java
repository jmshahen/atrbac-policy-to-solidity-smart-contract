package atrbac_ethereum;

import java.io.*;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import atrbac_ethereum.convert.BakedConverter;
import atrbac_ethereum.convert.GenericConverter;
import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.helper.*;
import mohawk.global.parser.mohawkT.MohawkTARBACParser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.timing.MohawkTiming;

public class ATRBACEthereumInstance {
    private final String VERSION = "v1.0.0";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen [AT] uwaterloo [DOT] ca>";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("shahen");
    private String Logger_filepath = "logs/ATRBACEthereum-Log.csv";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String timingFile = "logs/ATRBACEthereum-Timing.csv";

    // Helpers
    public MohawkTiming timing;
    public FileHelper fileHelper = new FileHelper();
    public ParserHelper parserHelper = new ParserHelper();

    // Settings
    /** 
    */
    public boolean debug = false;

    public int run(String[] args) {
        try {
            ////////////////////////////////////////////////////////////////////////////////
            // LOCAL VARIABLES
            timing = new MohawkTiming();
            // LOCAL VARIABLES
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // COMMANDLINE OPTIONS
            CommandLine cmd = init(args);
            if (cmd == null) { return 0; }

            // Only set tests.debug when equal to TRUE (this allows for default to be changed to TRUE)
            if (debug) {
                timing.printOnStop = true;
            }
            // COMMANDLINE OPTIONS
            ////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////
            // LOADING ALL Mohawk+T FILES
            /* Timing */timing.startTimer("loadFile");
            fileHelper.loadSpecFiles();
            /* Timing */timing.stopTimer("loadFile");
            // LOADING ALL Mohawk+T FILES
            ////////////////////////////////////////////////////////////////////////////////

            // Execute the test cases
            if (cmd.hasOption(ATRBACEthereumOptionString.RUN.toString())) {
                logger.info("[ACTION] Run paramter detected");

                String runVal = cmd.getOptionValue(ATRBACEthereumOptionString.RUN.toString());
                Integer numFiles = fileHelper.specFiles.size();

                ////////////////////////////////////////////////////////////////////////////////
                // INDIVIDUAL FILE LOOP
                logger.info("Mohawk+T Policy Files to Convert: " + fileHelper.printFileNames(300, true));
                for (Integer i = 1; i <= fileHelper.specFiles.size(); i++) {
                    ////////////////////////////////////////////////////////////////////////////////
                    /* TIMING */String timerName = "mainLoop (" + i + "/" + numFiles + ")";
                    /* TIMING */timing.startTimer(timerName);
                    File specFile = fileHelper.specFiles.get(i - 1);
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // PARSING MOHAWK+T FILE
                    /* Timing */timing.startTimer(timerName + "-parseFile (" + i + ")");
                    logger.info("Processing File (" + i + "/" + numFiles + "): " + specFile.getAbsolutePath());
                    MohawkTARBACParser parser = parserHelper.parseMohawkTFile(specFile);
                    /* Timing */timing.stopTimer(timerName + "-parseFile (" + i + ")");

                    MohawkT m = parser.mohawkT;
                    m.policyFilename = specFile.getAbsolutePath();

                    if (parserHelper.error.errorFound) {
                        logger.warning("[PARSING] ERROR: Skipping this file due to a parsing error");
                        continue;
                    } else {
                        logger.info("[PARSING] No errors found while parsing file, continuing on to converting");
                    }
                    // END OF PARSING MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    ////////////////////////////////////////////////////////////////////////////////
                    // RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    switch (runVal) {
                        case "baked" :
                            logger.info("[BAKED ACTION] "
                                    + "Will bake each rule from the ATRBAC policy as a unique function");

                            File solidityFile = new File(
                                    FileExtensions.swapExtension(specFile.getAbsolutePath(), ".sol"));
                            if (BakedConverter.convert(m, solidityFile)) {
                                logger.info("Converted file: " + solidityFile.getAbsolutePath());
                            } else {
                                logger.severe("Error converting ATRBAC file to Solidity using the BakedConverter");
                            }

                            break;
                        case "generic" :
                            logger.info("[GENERIC ACTION] "
                                    + "Will write a TRUFFLE test file to be run with the generic ATRBAC smart contract");

                            File truffleTestFile = new File(
                                    FileExtensions.swapExtension(specFile.getAbsolutePath(), ".js"));
                            if (GenericConverter.convert(m, truffleTestFile)) {
                                logger.info("Converted file: " + truffleTestFile.getAbsolutePath());
                            } else {
                                logger.severe(
                                        "Error converting ATRBAC file to Truffle Test File using the GenericConverter");
                            }

                            break;
                        default :
                            logger.severe("The Run Option '" + runVal + "' has not been implemented. "
                                    + "Please see use 'mohawk -help' to see which Run Options have been implemented");
                            /* TIMING */timing.cancelTimer(timerName);
                            continue;
                    }
                    // END OF RUN A CERTAIN TASK ON THE MOHAWK+T FILE
                    ////////////////////////////////////////////////////////////////////////////////

                    /* TIMING */timing.stopTimer(timerName);
                }
                // END OF INDIVIDUAL FILE LOOP
                ////////////////////////////////////////////////////////////////////////////////
            }

            logger.info("[EOF] ATRBAC Ethereum is done running");

            logger.info("[TIMING] " + timing);
            timing.writeOut(new File(timingFile), false);
        } catch (ParseException e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -2;
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            return -1;
        } finally {
            for (Handler h : logger.getHandlers()) {
                h.close();// must call h.close or a .LCK file will remain.
            }
        }
        return 0;
    }

    /* ********* FUNCTIONS ********* */
    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    public void printHelp(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(ATRBACEthereumOptionString.MAXW.toString())) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue(ATRBACEthereumOptionString.MAXW.toString()));
                printHelp(options, maxw);
            } catch (NumberFormatException e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new NumberFormatException("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "cree",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    public CommandLine init(String[] args) throws ParseException, SecurityException, IOException, Exception {
        Options options = new Options();
        setupOptions(options);
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = parser.parse(options, args);

        setupLoggerOptions(cmd, options);
        if (setupReturnImmediatelyOptions(cmd, options)) { return null; }

        setupUserPreferenceOptions(cmd, options);
        setupFileOptions(cmd, options);
        setupControlOptions(cmd, options);

        return cmd;
    }

    /** Adds all of the available options to input parameter.
     * 
     * @param options */
    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(ATRBACEthereumOptionString.HELP.toString(), false, "Print this message");
        options.addOption(ATRBACEthereumOptionString.VERSION.toString(), false,
                "Prints the version (" + VERSION + ") information");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create(ATRBACEthereumOptionString.LOGLEVEL.toString()));

        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create(ATRBACEthereumOptionString.LOGFILE.toString()));

        options.addOption(ATRBACEthereumOptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(ATRBACEthereumOptionString.MAXW.toString()));

        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(ATRBACEthereumOptionString.LINESTR.toString()));

        options.addOption(ATRBACEthereumOptionString.DEBUG.toString(), false,
                "Add this flag to get more ouput about the steps that are taken by Mohawk+T "
                        + "(drastically slows down performance)");

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the ATRBAC Spec file or Folder if the 'bulk' option is set").hasArg()
                .create(ATRBACEthereumOptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + fileHelper.fileExt + "'")
                .hasArg().create(ATRBACEthereumOptionString.SPECEXT.toString()));

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Functional Options
        options.addOption(ATRBACEthereumOptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");
        ///////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////////////////
        // Add Actionable Options
        options.addOption(OptionBuilder.withArgName("all|smv|diameter")
                .withDescription("Runs the whole model checker when equal to 'all'; "
                        + "Runs only the SMV conversion when equal to 'smv'; "
                        + "Calculates the diameter when equal to 'diameter'")
                .hasArg().create(ATRBACEthereumOptionString.RUN.toString()));
        ///////////////////////////////////////////////////////////////////////////////////////////
    }

    /** Sets up the options that do not process anything in detail, but returns a specific informative result.<br/>
     * Examples include:
     * <ul>
     * <li>Help
     * <li>Version
     * <li>Check NuSMV programs
     * </ul>
     * 
     * @param cmd
     * @param options
     * @return
     * @throws NumberFormatException */
    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws NumberFormatException {
        if (cmd.hasOption(ATRBACEthereumOptionString.HELP.toString()) == true || cmd.getOptions().length < 1) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(ATRBACEthereumOptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        return false;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.FINEST);// Default Level
        if (cmd.hasOption(ATRBACEthereumOptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(ATRBACEthereumOptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.WARNING);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(ATRBACEthereumOptionString.NOHEADER.toString())) {
            WriteCSVFileHeader = false;
        }

        // Set File Logger
        if (cmd.hasOption(ATRBACEthereumOptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(ATRBACEthereumOptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue(ATRBACEthereumOptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                Logger_filepath = "cree-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(cmd.getOptionValue(ATRBACEthereumOptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }

        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {

            File logfile = new File(Logger_filepath);

            if (!logfile.exists()) {
                logfile.getParentFile().mkdirs();

                logfile.createNewFile();
                if (WriteCSVFileHeader) {
                    FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                    writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                    writer.flush();
                    writer.close();
                }
            }

            fileHandler = new FileHandler(Logger_filepath, true);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    /** Looks for the following options and sets up the program accordingly:
     * <ul>
     * <li>{@link ATRBACEthereumOptionString#DEBUG}
     * </ul>
     * 
     * @param cmd
     * @param options */
    private void setupControlOptions(CommandLine cmd, Options options) {
        /////////////////////////////////////////////////////////////////////////
        // DEBUG FLAG
        if (cmd.hasOption(ATRBACEthereumOptionString.DEBUG.toString())) {
            debug = true;
        } else {
            debug = false;
        }
        logger.info("[Option] Debug Mode: " + ((debug) ? "ENABLED" : "DISABLED"));
        /////////////////////////////////////////////////////////////////////////
    }

    private void setupFileOptions(CommandLine cmd, Options options) {
        // Grab the SPEC file
        if (cmd.hasOption(ATRBACEthereumOptionString.SPECFILE.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File: "
                    + cmd.getOptionValue(ATRBACEthereumOptionString.SPECFILE.toString()));
            fileHelper.specFile = cmd.getOptionValue(ATRBACEthereumOptionString.SPECFILE.toString());
        } else {
            logger.fine("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(ATRBACEthereumOptionString.SPECEXT.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(ATRBACEthereumOptionString.SPECEXT.toString()));
            fileHelper.fileExt = cmd.getOptionValue(ATRBACEthereumOptionString.SPECEXT.toString());
        } else {
            logger.fine("[OPTION] Using the default SPEC File Extension: " + fileHelper.fileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(ATRBACEthereumOptionString.BULK.toString())) {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Enabled");
            fileHelper.bulk = true;
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
            fileHelper.bulk = false;
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(ATRBACEthereumOptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(ATRBACEthereumOptionString.MAXW.toString());
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(ATRBACEthereumOptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(ATRBACEthereumOptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }
}
