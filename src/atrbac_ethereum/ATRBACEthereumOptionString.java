package atrbac_ethereum;

public enum ATRBACEthereumOptionString {
    /** Displays the help message in the command prompt */
    HELP("help"),
    /** Displays a list of authors who have contributed to this project */
    AUTHORS("authors"),
    /** Displays the current version of this program */
    VERSION("version"),
    /** 
    */
    DEBUG("debug"),
    /** Sets the logging level for the console and the log file */
    LOGLEVEL("loglevel"),
    /** Indicates which file to store the log file in */
    LOGFILE("logfile"),
    /**  */
    NOHEADER("noheader"),
    /** Sets the maximum width of the console log. Use 0 for no word-wrapping. */
    MAXW("maxw"),
    /**  */
    LINESTR("linstr"),
    /** The input file/folder to try and solve */
    SPECFILE("input"),
    /**  */
    SPECEXT("ext"),
    /** Treats the {@link CreeSolverOptionString#SPECFILE} as a 
     * folder and uses {@link CreeSolverOptionString#SPECEXT} for the file extension to search for */
    BULK("bulk"),
    /** Sets the actions that will be performed on the input policies. The following options are available:
     * <ul>
     * <li>"all" -
     * <li>"" - 
     * </ul>
    */
    RUN("run");

    private String _str;

    private ATRBACEthereumOptionString(String s) {
        _str = s;
    }

    @Override
    public String toString() {
        return _str;
    }

    public String s() {
        return _str;
    }

    /** Returns the commandline equivalent with the hyphen and a space following: AUTHORS -> "-authors "
     * 
     * @return */
    public String c() {
        return "-" + _str + " ";
    }

    /** Returns the commandline equivalent with the hyphen and a space following: LOGLEVEL("debug") -> "-loglevel debug
     * "
     * 
     * @param param
     * @return */
    public String c(String param) {
        return "-" + _str + " " + param + " ";
    }
}
